// A javascript-enhanced crossword puzzle [c] Jesse Weisbeck, MIT/GPL 
(function($) {
	$(function() {
		// provide crossword entries in an array of objects like the following example
		// Position refers to the numerical order of an entry. Each position can have 
		// two entries: an across entry and a down entry
		var puzzleData = [
			 	{
					clue: "It is hard to get out of.",
					answer: "bed",
					position: 1,
					orientation: "across",
					startx: 3,
					starty: 1
				},
			 	{
					clue: "Likes to sleep in other peoples bed.",
					answer: "goldilocks",
					position: 2,
					orientation: "across",
					startx: 2,
					starty: 3
				},
				{
					clue: "Not daddybear",
					answer: "mummybear",
					position: 3,
					orientation: "across",
					startx: 1,
					starty: 5
				},
				{
					clue: "It sure was one",
					answer: "adventure",
					position: 4,
					orientation: "across",
					startx: 2,
					starty: 7
				},
				{
					clue: "A good breakfast",
					answer: "porridge",
					position: 5,
					orientation: "across",	
					startx: 2,
					starty: 9
				},
				{
					clue: "Not mummybear",
					answer: "daddybear",
					position: 6,
					orientation: "down",
					startx: 5,
					starty: 1
				}
			] 
	
		$('#puzzle-wrapper').crossword(puzzleData);
		
	})
	
})(jQuery)
