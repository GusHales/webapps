// A javascript-enhanced crossword puzzle [c] Jesse Weisbeck, MIT/GPL 
(function($) {
	$(function() {
		// provide crossword entries in an array of objects like the following example
		// Position refers to the numerical order of an entry. Each position can have 
		// two entries: an across entry and a down entry
		var puzzleData = [
			 	{
					clue: "He is the _.",
					answer: "bigbadwolf",
					position: 1,
					orientation: "across",
					startx: 1,
					starty: 1
				},
			 	{
					clue: "little red riding hood carried it",
					answer: "basket",
					position: 2,
					orientation: "down",
					startx: 1,
					starty: 1
				},
				{
					clue: "The hero",
					answer: "lumberjack",
					position: 3,
					orientation: "down",
					startx: 9,
					starty: 1
				},
				{
					clue: "She is a nice old lady",
					answer: "grandma",
					position: 4,
					orientation: "across",
					startx: 4,
					starty: 3
				},
				{
					clue: "The wolf stole them",
					answer: "glasses",
					position: 5,
					orientation: "down",	
					startx: 4,
					starty: 3
				},
				{
					clue: "Red riding hood wore one",
					answer: "cape",
					position: 6,
					orientation: "across",
					startx: 1,
					starty: 8
				},
				{
					clue: "It happened in the _",
					answer: "forest",
					position: 7,
					orientation: "across",
					startx: 6,
					starty: 5
				},
				{
					clue: "You pick them.",
					answer: "flowers",
					position: 8,
					orientation: "down",
					startx: 6,
					starty: 5
				},
				{
					clue: "The wolf had realy big _.",
					answer: "teeth",
					position: 9,
					orientation: "down",
					startx: 11,
					starty: 5
				}
			] 
	
		$('#puzzle-wrapper').crossword(puzzleData);
		
	})
	
})(jQuery)