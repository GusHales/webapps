$(document).ready(

	/** This function does the fading */
	function() {
		$('#puzzle-wrapper').hide();
		$('#puzzle-clues').hide();
		$('#game').hide();
		$('.learned').hide();
		$('.challenge-header-extra').hide();
		$('.challenge-header-game').hide();

		$('#challenge').click(function() {
			$('.challenge-header').fadeOut(1500);
			$('#confirm').fadeOut(1500, function() {
				$('.challenge-header-extra').fadeIn(1500);
				$('.learned').fadeIn(1500);
				$('#game').fadeIn(1500);
			});
		});

		$('#learn').click(function() {
			$('#game').fadeOut(1500);
			$('.challenge-header-extra').fadeOut(1500);
			$('.learned').fadeOut(1500, function() {
				$('#puzzle-wrapper').fadeIn(1500);
				$('#puzzle-clues').fadeIn(1500);
				$('.challenge-header-game').fadeIn(1500);
			});
		});

    }

);