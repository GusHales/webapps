// A javascript-enhanced crossword puzzle [c] Jesse Weisbeck, MIT/GPL 
(function($) {
	$(function() {
		// provide crossword entries in an array of objects like the following example
		// Position refers to the numerical order of an entry. Each position can have 
		// two entries: an across entry and a down entry
		var puzzleData = [
			 	{
					clue: "And he ____",
					answer: "puffed",
					position: 1,
					orientation: "across",
					startx: 6,
					starty: 1
				},
			 	{
					clue: "The Big Bad Wolf lacked this",
					answer: "politeness",
					position: 1,
					orientation: "down",
					startx: 6,
					starty: 1
				},
				{
					clue: "The last little pigs house was made of this",
					answer: "brick",
					position: 2,
					orientation: "across",
					startx: 4,
					starty: 4
				},
				{
					clue: "The Big Bad Wolf done this first",
					answer: "knocked",
					position: 3,
					orientation: "across",
					startx: 1,
					starty: 6
				},
				{
					clue: "He ___",
					answer: "huffed",
					position: 4,
					orientation: "across",	
					startx: 2,
					starty: 8
				},
				{
					clue: "The first little pigs house was made of this	",
					answer: "hay",
					position: 2,
					orientation: "down",
					startx: 2,
					starty: 8
				},
				{
					clue: "The second little pigs house was made of this",
					answer: "sticks",
					position: 5,
					orientation: "across",
					startx: 6,
					starty: 10
				},
				{
					clue: "The three little pigs were ___",
					answer: "brothers",
					position: 3,
					orientation: "down",
					startx: 11,
					starty: 3
				},
				{
					clue: "The three little pigs did this when the house blew down",
					answer: "run",
					position: 4,
					orientation: "down",
					startx: 2,
					starty: 4
				}
			] 
	
		$('#puzzle-wrapper').crossword(puzzleData);
		
	})
	
})(jQuery)