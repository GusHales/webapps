<?php
	// This determines if a user is logged-in or not
	if(!isset($_SESSION) || empty($_SESSION['username'])) {
		print("<li><a href='../PHP/LogIn/login.php'>Log In</a></li>");
	} else {
		print("<li><a href='../HTML/userhome.php'> {$_SESSION['username']} </a></li>");
	}
?>
