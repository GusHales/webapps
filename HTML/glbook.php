<?php session_start(); ?>

<html>
	<head>
		<title>eBooky - An interactive learning experience</title> 
		<script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
		<script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script src="../JavaScript/Crossword/jquery.crossword.js"></script>
		<script type="text/javascript" src="../JavaScript/book.js"></script>
		<script type="text/javascript" src="../JavaScript/goldScript.js"></script>
		<link rel="shortcut icon" href="../images/book_blue.ico"/>
		<link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../CSS/home.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../CSS/crossword.css">
		<link href="../booklet/jquery.booklet.latest.css" type="text/css" rel="stylesheet" media="screen, projection, tv" />
	</head>

	<body>

		<div id="bg">
  			<img src="../images/cottage.jpg" alt="">
		</div>

		<nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
			<div class="container">
		    	<a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			    <ul class="nav navbar-nav navbar-right">
			        <?php include '../PHP/session.php'; ?>
			        <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="aboutUs.php">About us</a></li>
			            <li><a href="contactUs.php">Contact us</a></li>
			            <li><a href="bookinfo.php">Book Information</a></li>
			            <li><a href="tlpcomments.php">Comment a Book</a></li>
			          </ul>
			        </li>
			      </ul>

			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-collapse -->
		</nav>
		<section>
	    <div id="mybook">
	        <div title="first page">
	            <img border="0" src="../Goldilocks/page1.jpg" alt="Page 1" width="270" height="370">
	        </div>
	        <div title="second page">
	            <img border="0" src="../Goldilocks/page2.jpg" alt="Page 2" width="270" height="370">
	        </div>
	        <div title="third page">
	            <img border="0" src="../Goldilocks/page3.jpg" alt="Page 3" width="270" height="370">
	        </div>
	        <div title="fourth page">
	            <img border="0" src="../Goldilocks/page4.jpg" alt="Page 4" width="270" height="370">
	        </div>
	        <div title="fifth page">
	            <img border="0" src="../Goldilocks/page5.jpg" alt="Page 5" width="270" height="370">
	        </div>
	        <div title="sixth page">
	            <img border="0" src="../Goldilocks/page6.jpg" alt="Page 6" width="270" height="370">
	        </div>
	        <div title="seventh page">
	            <img border="0" src="../Goldilocks/page7.jpg" alt="Page 7" width="270" height="370">
	        </div>
	        <div title="eighth page">
	            <img border="0" src="../Goldilocks/page8.jpg" alt="Page 8" width="270" height="370">
	        </div>
	        <div title="ninth page">
	            <img border="0" src="../Goldilocks/page9.jpg" alt="Page 9" width="270" height="370">
	        </div>
	        <div title="tenth page">
	            <img border="0" src="../Goldilocks/page10.jpg" alt="Page 10" width="270" height="370">
	        </div>
	        <div title="eleventh page">
	            <img border="0" src="../Goldilocks/page11.jpg" alt="Page 11" width="270" height="370">
	        </div>
	    </div>
	</section>

	<div class="challenge-header">
		<h2>Ready for a Challenge?</h2>
	</div>

	<div id="confirm" class="button-wrapper">
    	<button type="button" id="challenge" class="btn btn-primary btn-cons">Yes!</button>
    </div>

    <div class="challenge-header-extra">
		<h2>What did we learn?</h2>
	</div>

	<div class="learned">
		<ul>
			<li>Always be polite!</li>
			<li>Respect what other people think!</li>
			<li>Be friendly!</li>
			<li>Don't be rude!</li>
			<li>Stay in school!</li>
			<li>Don't do drugs!</li>
		</ul>
	</div>

	<div id="game" class="button-wrapper" >
    	<button type="button" id="learn" class="btn btn-primary btn-cons">Done!</button>
    </div>

	<div class="challenge-header-game">
		<h2>Crossword!</h2>
	</div>

	<div id="puzzle-wrapper"></div>

	<footer></footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script> window.jQuery || document.write('<script src="../booklet/jquery-2.1.0.min.js"><\/script>') </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script> window.jQuery.ui || document.write('<script src="../booklet/jquery-ui-1.10.4.min.js"><\/script>') </script>
    <script src="../booklet/jquery.easing.1.3.js"></script>
    <script src="../booklet/jquery.booklet.latest.js"></script>
	<script>
	    $(function () {		
	        $("#mybook").booklet();
	    });
    </script>
	</body>

</html>