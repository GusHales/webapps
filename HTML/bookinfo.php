<?php session_start(); ?>

<html>
	<head>
		<title>eBooky - An interactive learning experience</title> 
		<script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
		<script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>

		<script>
			function showUser(str) {
  				if (str=="") {
    				document.getElementById("txtHint").innerHTML="";
    				return;
  				}
  				if (window.XMLHttpRequest) {
    				// code for IE7+, Firefox, Chrome, Opera, Safari
    				xmlhttp=new XMLHttpRequest();
  				} else { // code for IE6, IE5
    				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  				}
  				
  				xmlhttp.onreadystatechange=function() {
    				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      					document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    				}
  				}
  			xmlhttp.open("GET","getbookinfo.php?q="+str,true);
 		 	xmlhttp.send();
		}
		</script>

		<link rel="shortcut icon" href="../images/book_blue.ico"/>
		<link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../CSS/home.css">
	</head>

	<body>

		<div id="bg">
  			<img src="../images/background.png" alt="">
		</div>

		<nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
			<div class="container">
		    	<a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			    <ul class="nav navbar-nav navbar-right">
			    <?php include '../PHP/session.php'; ?>
			        <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="aboutUs.php">About us</a></li>
			            <li><a href="contactUs.php">Contact us</a></li>
			            <li><a href="bookinfo.php">Book Information</a></li>
			            <li><a href="tlpcomments.php">Comment a Book</a></li>
			          </ul>
			        </li>
			      </ul>

			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-collapse -->
		</nav>

		<div class="left-pane-info">	
			<div class="info-body">
				<h1>Want to find out more ?</h1>
			</div>

			<div class="left-pane-info">
				
			<form>
				<select name="users" onchange="showUser(this.value)">
				<option value="">Select a book:</option>
				<option value="1">Three Little Pigs</option>
				<option value="2">Little Red Riding Hood</option>
				<option value="3">Goldilocks</option>
				</select>
			</form>
<br>
<div id="txtHint"><b>Book info will be listed here.</b></div>

			</div>
		</div>
			
	</body>

</html>