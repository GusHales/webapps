<?php session_start(); ?>

<html>
    <head>
        <title>eBooky - An interactive learning experience</title> 
        <script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
        <script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>
        <!-- <script type="text/javascript" src="../JavaScript/welcome_script.js"></script> -->
        <link rel="shortcut icon" href="../images/book_blue.ico"/>
        <link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../CSS/home.css">
    </head>

    <body>
 
        <div id="bg">
            <img src="../images/background.png" alt="">
        </div>

        <nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
            <div class="container">
                <a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <?php include '../PHP/session.php'; ?>
                    <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">More<b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="aboutUs.php">About us</a></li>
                        <li><a href="contactUs.php">Contact us</a></li>
                        <li><a href="bookinfo.php">Book Information</a></li>
                        <li><a href="tlpcomments.php">Comment a Book</a></li>
                      </ul>
                    </li>
                  </ul>

                </div>
            </div>
        </nav>


       <div class="message">
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">                
                        <form action="addEntry.php" method="POST">
                            <textarea class="form-control counted" name="entryText" placeholder="Type in your message" rows="10" cols="28" style="margin-bottom:10px;"></textarea>
                            <button class="btn btn-info" type="submit">Post New Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div id="content-wrapper">
            <div class="body-elements">
                <div class="right-wing">
                    <?php include '../HTML/index.php'; ?>
                </div>
            </div>
        </div>

    </div>
</div>
  

</div>
</html>
