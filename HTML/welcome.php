<?php session_start(); ?>

<html>
	<head>
		<title>eBooky - An interactive learning experience</title> 
		<script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
		<script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>
		<link rel="shortcut icon" href="../images/book_blue.ico"/>
		<link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../CSS/home.css">
	</head>

	<body>

		<div id="bg">
  			<img src="../images/background.png" alt="">
		</div>

		<nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
			<div class="container">
		    	<a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			    <ul class="nav navbar-nav navbar-right">
			    <?php include '../PHP/session.php'; ?>
			        <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="aboutUs.php">About us</a></li>
			            <li><a href="contactUs.php">Contact us</a></li>
			            <li><a href="bookinfo.php">Book Information</a></li>
			            <li><a href="tlpcomments.php">Comment a Book</a></li>
			          </ul>
			        </li>
			      </ul>

			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-collapse -->
		</nav>

		<div class="body-elements">	
			<div class="body-header">
				<h1>What is eBooky?</h1>
			</div>

			<div class="left-pane">
				<p>
					This app takes an already well-established children's book and create an engaging app for children and an educational resource. Our main selling point is teaching children the importance of inclusion and individually as bullying becomes more and more of an issue. Not only would the app hold the engaging storyline, but we would also include games, puzzles and educational resources for children to engage in, all holding the same message. We believe it to be vibrant, colourful and creative. We already have numerous pictures, ideas and games we would choose to use, as well as potential customers, parents, teachers and children alike. 
				</p>
			</div>
		</div>
			<div class="right-pane">
				
				<div id="myCarousel" class="carousel slide">
  					<ol class="carousel-indicators">
   						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
   						<li data-target="#myCarousel" data-slide-to="1"></li>
    					<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>
					<!-- Carousel items -->
					<div class="carousel-inner">
					  	<div class="active item">
                      		<img src="../images/PuzzleTest.jpeg" alt="">
                      		<div class="carousel-caption">
                      			<h4>Three Little Pigs</h4>
                     			<p>Can you save them from the Big Bad Wolf?</p>
                     		</div>
                     	</div>
                      
                      	<div class="item">
                      		<img src="../images/LRH2.jpg" alt="">
                      		<div class="carousel-caption">
                      			<h4>Little Red Riding Hood</h4>
                      			<p>Is it really Grandma?</p>
                     		</div>
                    	</div>
					  	
					  	<div class="item">
                      		<img src="../images/GL.jpg" alt="">
                      		<div class="carousel-caption">
                      			<h4>Goldilocks</h4>
                      			<p>Will she get away with the porridge?</p>
                     		</div>
                     	</div>
                    </div>
				</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="carousel-control right" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>
			
		</div>
	</body>

</html>