<?php session_start(); ?>
<html>
	<head>
		<title>eBooky - An interactive learning experience</title> 
		<script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
		<script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script src="../JavaScript/Crossword/jquery.crossword.js"></script>
		<script type="text/javascript" src="../JavaScript/book.js"></script>
		<script type="text/javascript" src="../JavaScript/redScript.js"></script>
		<link rel="shortcut icon" href="../images/book_blue.ico"/>
		<link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../CSS/home.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../CSS/crossword.css">
		<link href="../booklet/jquery.booklet.latest.css" type="text/css" rel="stylesheet" media="screen, projection, tv" />
	</head>

	<body>

		<div id="bg">
  			<img src="../images/path-in-the-woods-8451.jpg" alt="">
		</div>

		<nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
			<div class="container">
		    	<a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			    <ul class="nav navbar-nav navbar-right">
			        <?php include '../PHP/session.php'; ?>
			        <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="aboutUs.php">About us</a></li>
			            <li><a href="contactUs.php">Contact us</a></li>
			            <li><a href="bookinfo.php">Book Information</a></li>
			            <li><a href="tlpcomments.php">Comment a Book</a></li>
			          </ul>
			        </li>

			      </ul>

			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-collapse -->
		</nav>

		<section>
	    <div id="mybook">
	        <div title="first page">
	            <p>Once upon a time there was a dear little girl who was loved by everyone who looked at her but most of all by her grandmother and there was nothing that she would not have given to the child. Once she gave her a little riding hood of red velvet, which suited her so well that she would never wear anything else. So she was always called "Little Red Riding Hood."
    			   One day her mother said to her, \"'Come, Little Red Riding Hood.  Here is a piece of cake and a bottle of wine.  Take them to your grandmother she is ill and weak and they will do her good.
                   Set out before it gets hot. 
     			</p>
	        </div>
	        <div title="second page">
	            <p>When you are going, walk nicely and quietly and do not run off the path or you may fall and break the bottle and then your grandmother will get nothing.  When you go into her room don't forget to say, ‘Good morning.’ Don't peep into every corner before you do it."
			     “I will take great care," said Little Red Riding Hood to her mother.
			</p>
	        </div>
	        <div title="third page">
	             <p> The grandmother lived out in the woods. Just as Little Red Riding Hood entered the woods a wolf met her. Red Riding Hood did not know what a wicked creature he was and was not at all afraid of him.
			     “Good day, Little Red Riding Hood," said he.
			     “Thank you kindly, wolf."
			     “Where are you going so early?"
			     “To my grandmother's."
				 “What have you got in your basket?"
				     “Cake and wine."
				     "Where does your grandmother live, Little Red Riding Hood?"</p>
	        </div>
	        <div title="fourth page">
	            <p>   
				     “Her house stands under the three large oak-trees and the nut-trees are just below.  You surely must know it." replied Little Red Riding Hood.
				       She was surprised to find the cottage-door standing open. When she went into the room she had such a strange feeling. She called out, “Good morning," but received no answer.  So she went to the bed and drew back the curtains. There lay her grandmother with her cap pulled far over her face and looking very strange.
				     “Oh! Grandmother," she said, “What big ears you have!"
				     “All the better to hear you with, my child," was the reply.</p>
	        </div>
	        <div title="fifth page">
	            <p>    “But, grandmother, what big eyes you have!" she said.
				     “All the better to see you with, my dear."
				     “But, grandmother, what large hands you have!"
				     “All the better to hug you with."
				  “Oh! But, grandmother, what a big mouth you have!"
				     “All the better to eat you with!"</p>
	        </div>
	        <div title="sixth page">
	            <p>   Scarcely had the wolf said this, than with one bound he was out of bed and swallowed up Red Riding Hood.
				     When the wolf had appeased his appetite, he lay down again in the bed, fell asleep and began to snore very loud.
				     The huntsman was just passing the house and thought to himself, “How the old woman is snoring! I must just see if she wants anything."
				       So he went into the room and when he came to the bed, he saw the wolf.</p>
	        </div>
	        <div title="seventh page">
	            <p> Just as he was going to fire at the wolf, it occurred to him that the wolf might have devoured the grandmother and that she might still be saved.  So he did not fire but took a pair of scissors and began to cut open the stomach of the sleeping wolf.
			       When he had made two snips he saw the Little Red Riding Hood and then he made two snips and the little girl sprang out, crying, “Ah, how frightened I have been! How dark it was inside the wolf."</p>
	        </div>
	        <div title="eighth page">
	            <p>After that Grandmother came out alive but scarcely able to breathe. Red Riding Hood quickly fetched great stones with which they filled the wolf's belly. When he awoke he wanted to run away but the stones were so heavy that he collapsed at once and fell dead.
		       Little Red Riding Hood sat down with her grandmother and the huntsman and together they ate the cake Little Red Riding Hood had brought.</p>
	        </div>
	    </div>
	</section>

	<div class="challenge-header">
		<h2>Ready for a Challenge?</h2>
	</div>

	<div id="confirm" class="button-wrapper">
    	<button type="button" id="challenge" class="btn btn-primary btn-cons">Yes!</button>
    </div>

    <div class="challenge-header-extra">
		<h2>What did we learn?</h2>
	</div>

	<div class="learned">
		<ul>
			<li>Always be polite!</li>
			<li>Respect what other people think!</li>
			<li>Be friendly!</li>
			<li>Don't be rude!</li>
			<li>Stay in school!</li>
			<li>Don't do drugs!</li>
		</ul>
	</div>

	<div id="game" class="button-wrapper" >
    	<button type="button" id="learn" class="btn btn-primary btn-cons">Done!</button>
    </div>

	<div class="challenge-header-game">
		<h2>Crossword!</h2>
	</div>

	<div id="puzzle-wrapper"></div>

	<footer></footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script> window.jQuery || document.write('<script src="../booklet/jquery-2.1.0.min.js"><\/script>') </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script> window.jQuery.ui || document.write('<script src="../booklet/jquery-ui-1.10.4.min.js"><\/script>') </script>
    <script src="../booklet/jquery.easing.1.3.js"></script>
    <script src="../booklet/jquery.booklet.latest.js"></script>

	<script>
	    $(function () {		
	        $("#mybook").booklet();
	    });
    </script>
	</body>

</html>