<?php session_start(); ?>

<html>
	<head>
		<title>eBooky - An interactive learning experience</title> 
		<script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
		<script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>
		<link rel="shortcut icon" href="../images/book_blue.ico"/>
		<link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../CSS/home.css">
	</head>

	<body>

		<div id="bg">
  			<img src="../images/background.png" alt="">
		</div>

		<nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
			<div class="container">
		    	<a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			    <ul class="nav navbar-nav navbar-right">
			    <?php include '../PHP/session.php'; ?>
			        <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="aboutUs.php">About us</a></li>
			            <li><a href="contactUs.php">Contact us</a></li>
			            <li><a href="bookinfo.php">Book Information</a></li>
			            <li><a href="tlpcomments.php">Comment a Book</a></li>
			          </ul>
			        </li>
			      </ul>

			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-collapse -->
		</nav>
		
		<div class="container" id="user">
		    <div class="row">
				<div class="col-md-5 col-lg-5">
					<!-- artigo em destaque -->
					<div class="featured-article">
						<a href="tlpbook.php">
							<img src="../images/tlpcover.png" alt="" class="thumb">
						</a>
						<div class="block-title">
							<h2>Three Little Pigs Book</h2>
							<p class="by-author"><small>Retold By Alyse Sweeney</small></p>
						</div>
					</div>
					<!-- /.featured-article -->
				</div>
				<div class="col-md-7 col-lg-7">
					<ul class="media-list main-list">
					  <li class="media">
					    <a class="pull-left" href="lrrhbook.php">
					      <img class="media-object" src="../images/lrrhcover.png" alt="...">
					    </a>
					    <div class="media-body">
					      <h4 class="subtitle">Litter Red Riding Hood</h4>
					      <p class="by-author">By ...</p>
					    </div>
					  </li>
					  <li class="media">
					    <a class="pull-left" href="glbook.php">
					      <img class="media-object" src="../images/glcover.png" alt="...">
					    </a>
					    <div class="media-body">
					      <h4 class="subtitle">Goldilocks</h4>
					      <p class="by-author">By Emma Clark</p>
					    </div>
					  </li>
					</ul>
				</div>
			</div>
</html>