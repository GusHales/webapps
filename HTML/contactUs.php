<?php session_start(); ?>

<html>
	<head>
		<title>eBooky - An interactive learning experience</title> 
		<script type="text/javascript" src="../JavaScript/jQuery_1.9.js"></script>
		<script type="text/javascript" src="../JavaScript/js/bootstrap.js"></script>
		<link rel="shortcut icon" href="../images/book_blue.ico"/>
		<link rel="stylesheet" type="text/css" href="../CSS/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../CSS/home.css">
	</head>

	<body>

		<div id="bg">
  			<img src="../images/background.png" alt="">
		</div>

		<nav class="navbar navbar-default navbar-static-top" id="nav" role="navigation">
			<div class="container">
		    	<a href="welcome.php"><img class="navbar-brand" src="../images/logo.png"></a>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			    <ul class="nav navbar-nav navbar-right">
			    <?php include '../PHP/session.php'; ?>
			        <li><a href="../PHP/LogIn/logout.php">Log Out</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="aboutUs.php">About us</a></li>
			            <li><a href="contactUs.php">Contact us</a></li>
			            <li><a href="bookinfo.php">Book Information</a></li>
			            <li><a href="tlpcomments.php">Comment a Book</a></li>
			          </ul>
			        </li>
			      </ul>

			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-collapse -->
		</nav>

		<div class="body-elements">	
			<div class="body-header">
				<h1>Contact Us</h1>
			</div>

			<div class="left-pane">
				<p>
					If you would like to reach us please feel free to drop us an email  </br>
					</br>

					The Team:
					<br>Barry McAuley: barrymcauley92@gmail.com</br>
					<br>David Forbes: toastietmg@gmail.com</br>
					<br>Gus Hales: agustin_hales@hotmail.com</br>
				</p>
			</div>
		</div>
	</body>

</html>